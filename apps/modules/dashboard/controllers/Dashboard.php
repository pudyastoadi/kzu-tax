<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Dashboard
 *
 * @author adi
 */
class Dashboard extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
        );
        $this->load->model('dashboard_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index()
    {    
        $this->_init_add();
        $this->template
            ->title('Dashboard',$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }  

    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Awal',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => date('Y-01'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
           'periode_akhir'=> array(
                    'placeholder' => 'Periode',
                    'id'          => 'periode_akhir',
                    'name'        => 'periode_akhir',
                    'value'       => date('Y-m'),
                    'class'       => 'form-control',
                    'required'    => '',
            ),
        );
    }    
}
