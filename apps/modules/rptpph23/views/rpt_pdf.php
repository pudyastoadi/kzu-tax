<style> 
    .code {
        background-color: #dadada; 
        box-shadow: 5px 4px #000;
        margin-left: 20%;
        margin-right: 20%;
        margin-bottom: 10px;
    }

    font.border{
        border:1px solid #000; 
    }

    font.no-border{
        width: 10px;
    }

    span.desc{
        font-weight: normal;
        font-size: 8px;
    }
    td{
        font-family: 'FreeSans';
    }
</style>
<table border="0" class="" width="100%">
    <tr>
        <td colspan="4">
            <table width="100%">
                <tr>
                    <td style="background-color: #000;height:12px;padding: 0px;">
                        
                    </td>
                    <td style="width:90%;padding: 0px;">

                    </td>
                    <td style="background-color: #000;padding: 0px;">
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td style="text-align: center;font-weight: bold;">
            
        </td>
        <td width="60px;">
            
        </td>
        <td valign="top" width="230px;">
            <table style="font-weight: bold; font-size: 10px;">
                <tr>
                    <td>
                        Lembar ke-1 untuk : Wajib Pajak<br>
                        Lembar ke-2 untuk : Kantor Pelayanan Pajak<br>
                        Lembar ke-3 untuk : Pemotong Pajak
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <img src="<?=base_url('assets/dist/img/logo-tax-mini.jpg');?>" style="height:75px;" />
        </td>
        <td style="text-align: center;font-weight: bold;font-size: 12.5px;">
            DEPARTEMEN KEUANGAN REPUBLIK INDONESIA
            <br>
            DIREKTORAT JENDRAL PAJAK
            <br>
            KANTOR PELAYANAN PAJAK
            <br>
            <?=strtoupper($nama_kpp);?>
        </td>
        <td>
            
        </td>
        <td valign="top">
            
        </td>
    </tr>
</table>

<div class="code">
    <table border="1" width="100%" style="border-collapse: collapse;">
        <tr>
            <td style="text-align: center;background: #dadada; font-weight: bold;height: 30px;">
                BUKTI PEMOTONGAN PPh PASAL 23
            </td>
        </tr>
        <tr>
            <td style="text-align: center;background: #dadada; font-weight: bold;padding: 5px;padding-bottom:13px;">
                Nomor : <?=$pph23['nb_dp'];?>
            </td>
        </tr>
    </table>
</div>

<table border="0" class="" width="100%">
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="100px;">
            NPWP
        </td>
        <td width="10px;">
            :
        </td>
        <td>
            <?php echo format_npwp($pph23['npwp_wp_dp']); ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="100px;">
            Nama WP
        </td>
        <td width="10px;">
            :
        </td>
        <td>
            <?php echo format_nmwp($pph23['nm_wp_dp']); ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="100px;">
            Alamat
        </td>
        <td width="10px;">
            :
        </td>
        <td>
            <?php
                echo format_nmwp($pph23['alamat_wp_dp']);
            ?>
        </td>
    </tr>
</table>

<table border="1" style="border-collapse: collapse;margin-top:10px;" width="100%">
    <tr style="background: #dadada;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:40px;">
            No
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">
            Jenis Penghasilan
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:170px;">
            Jumlah Penghasilan
            <br>
            Bruto (Rp)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:120px;">
            Tarif Lebih Tinggi
            <br>
            100% (Tdk ber-NPWP)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:70px;">
            Tarif
            <br>
            %
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:170px;">
            PPh yang Dipotong
            <br>
            (Rp)
        </td>
    </tr>
    <tr style="background: #dadada;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(1)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(2)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(3)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(4)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(5)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(6)</td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;" valign="top">1.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;" valign="top">Deviden *) </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_1']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_1']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_1']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top">2.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">Bunga **) </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_2']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_2']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_2']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top">3.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">Royalti</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_3']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_3']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_3']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top">4.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">Hadiah dan penghargaan</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_4']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_4']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_4']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top">5.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            Sewa dan Penghasilan lain sehubungan dengan
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            penggunaan harta ***)
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_5']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_5']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_5']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top">6.</td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            Jasa Teknik, Jasa Manajemen, Jasa Konsultasi dan Jasa Lain sesuai PMK-224/PMK.03/2008 :
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            a. Jasa Teknik
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6a']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6a']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6a']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            b. Jasa Manajemen
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6b']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6b']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6b']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            c. Jasa Konsultan
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6c']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6c']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6c']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;border-bottom:none;border-top:none;" valign="top">
            d. Jasa lain :
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;" valign="top"></td>
    </tr>
    <tr style="background: #fff;min-height:100px;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:45px;" valign="top">
            1.) <span class="desc"><?=($pph23['nm_kj_6d1']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6d1']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6d1']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6d1']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:45px;" valign="top">
            2.) <span class="desc"><?=($pph23['nm_kj_6d2']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6d2']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6d2']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6d2']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:45px;" valign="top">
            3.) <span class="desc"><?=($pph23['nm_kj_6d3']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6d3']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6d3']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6d3']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:45px;" valign="top">
            4.) <span class="desc"><?=($pph23['nm_kj_6d4']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6d4']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6d4']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6d4']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:45px;" valign="top">
            5.) <span class="desc"><?=($pph23['nm_kj_6d5']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['nb_6d5']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;" valign="top"><strong><?=persen($pph23['trf_6d5']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;" valign="top"><?=rupiah($pph23['pph_dp_6d5']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-bottom:none;border-top:none;" valign="top"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-bottom:none;border-top:none;height:30px;" valign="top">
            6.) <span class="desc"><?=($pph23['nm_kj_6d6']);?></span>
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;border-bottom:none;" valign="top"><?=rupiah($pph23['nb_6d6']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;border-bottom:none;" valign="top"></td>
        <td style="text-align:center;font-size: 12px;border-bottom:none;" valign="top"><strong><?=persen($pph23['trf_6d6']);?></strong> %</td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;border-bottom:none;" valign="top"><?=rupiah($pph23['pph_dp_6d6']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none;" valign="bottom"></td>
        <td style="font-weight: normal;text-align:left;font-size: 12px;border-top:none;height:30px;" valign="bottom">
            ****)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none;"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none;"></td>
        <td style="text-align:center;font-size: 12px;border-top:none;"></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none;"></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none;height:30px;" colspan="2">
            JUMLAH
        </td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;"><?=rupiah($pph23['jml_bruto']);?></td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;background: #dadada;"></td>
        <td style="text-align:center;font-size: 12px;background: #dadada;"></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;"><?=rupiah($pph23['pph_dp']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: normal;text-align:left;font-size: 13px;border-top:none;height:30px;padding: 10px;" colspan="6" valign="top">
            <strong>Terbilang: </strong> <?=ucwords(terbilang($pph23['pph_dp'])." Rupiah");?>
        </td>
    </tr>
</table>

<table border="1" style="border-collapse: collapse;margin-top:10px;" width="100%">
    <tr>
        <td style="font-weight: bold;text-align:left;font-size: 12px;">
            <table>
                <tr>
                    <td colspan="2" style="font-weight: bold;">    
                        Perhatian : 
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-weight: bold;">
                        1.
                    </td>
                    <td style="font-weight: bold;font-size: 13px;">
                        Jumlah Pajak Penghasilan Pasal 23
                        yang dipotong di atas merupakan
                        Angsuran atas pihak Penghasilan yang
                        bersangkutan. Simpanlah bukti 
                        pemotongan ini baik-baik untuk
                        diperhitungkan sebagai kredit pajak
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-weight: bold;">
                        2.
                    </td>
                    <td style="font-weight: bold;font-size: 13px;">
                        Bukti Pemotongan ini dianggap sah
                        apabila diisi dengan lengkap dan
                        benar.
                    </td>
                </tr>
            </table>
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:490px;border: none;" valign="top">
            <?php
                $tgl = explode("-", $pph23['tg_dp']);
            ?>
            <?=strtoupper($lokasi_kpp);?>,  <?=$tgl[2]." ".month($tgl[1])." ".$tgl[0];?>
            <table width="100%" style="padding-top:10px;">
                <tr>
                    <td colspan="3" style="font-weight: bold;">Pemotong Pajak,</td>
                </tr>  
                <tr>
                    <td style="text-align: left;width: 70px;">NPWP</td>
                    <td style="width: 1px;">:</td>
                    <td><?php echo format_npwp_footer('014460810511000');?></td>
                </tr>   
                <tr>
                    <td style="text-align: left;width: 70px;">Nama</td>
                    <td style="width: 1px;">:</td>
                    <td><?php echo format_nmwp('PT. KARYA ZIRANG UTAMA',19);?></td>
                </tr>   
                <tr>
                    <td style="text-align: left;width: 70px;"></td>
                    <td style="width: 1px;"></td>
                    <td>
                        <img src="<?=base_url('assets/dist/img/ttd.png');?>" style="max-width: 250px;max-height: 110px;position: relative;left: 35px;"/>
                    </td>
                </tr>               
            </table>
        </td>
    </tr>
</table>

<table style="margin-top:10px;" width="100%"  >
    <tr>
        <td style="font-weight: bold;text-align:left;font-size: 12px;width:58%;">
            <table width="100%" style="font-style: italic;">
                <tr>
                    <td valign="top" style="text-align:right;font-weight: bold;font-size: 12px;width:40px;">
                        *)
                    </td>
                    <td style="font-weight: bold;font-size: 12px;">
                        Tidak termasuk deviden kepada WP Orang Pribadi dalam negeri
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align:right;font-weight: bold;font-size: 12px;width:40px;">
                        **)
                    </td>
                    <td style="font-weight: bold;font-size: 12px;">
                        Tidak termasuk harga simpanan yang dibayarkan oleh koperasi.
                        kepada anggota WP Orang Pribadi
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align:right;font-weight: bold;font-size: 12px;width:40px;">
                        ***)
                    </td>
                    <td style="font-weight: bold;font-size: 12px;">
                        Kecuali sewa tanah dan bangunan
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align:right;font-weight: bold;font-size: 12px;width:40px;">
                        ****)
                    </td>
                    <td style="font-weight: bold;font-size: 12px;">
                        Apabila kurang harap diisi sendiri
                    </td>
                </tr>
            </table>            
        </td>
        <td style="font-weight: normal;text-align:center;font-size: 14px;" valign="top">
            <br>
            <strong>NUR KHAYATI</strong>
            <br>
            ACCOUNTING MANAGER  
        </td>
        <td style="font-weight: normal;text-align:center;font-size: 14px;">
            &nbsp;
        </td>
    </tr>
</table>