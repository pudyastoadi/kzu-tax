<style> 
    .code {
        background-color: #dadada; 
        box-shadow: 5px 4px #000;
        margin-left: 20%;
        margin-right: 20%;
        margin-bottom: 10px;
    }

    font.border{
        border:1px solid #000; 
    }

    font.no-border{
        width: 10px;
    }

    span.desc{
        font-weight: normal;
        
    }
    td{
        font-family:  Arial;
    }
</style>
<table    width="82%"  style="border-collapse: collapse;margin-top:0px;" >
    <tr >
      <td width="337"  rowspan="2" style="text-align: center; font-weight: bold; font-size: 18px; border-top: none; border-bottom: none; border-left: none; width:200px;" >
            <img src="<?=base_url('assets/dist/img/logo21.jpg');?>" style="height:200px;" />
          </td>
		
		
          <td height=170px colspan="6" ><img src="<?=base_url('assets/dist/img/logo21b.jpg');?>" width="670" height=170px  /></td>
  </tr> 
    
	<tr>
	  <td width="5" height="27" style="text-align: center; font-weight: bold; font-size: 12px;  border-bottom: none; border-left: none; border-right: none;" >&nbsp;</td>
	  <td width="99" style="text-align: center; font-weight: bold; font-size: 14px;  border-bottom: none; border-left: none; border-right: none;" >NOMOR:</td>
	  <td width="33" style="text-align: center; font-weight: bold; font-size: 9px;  border-bottom: none; border-left: none; border-right: none;"><font color="#57DDF9"; >H.01</font></td>
	  <td width="402" style="text-align: left; font-weight: bold; font-size: 14px;  border-bottom: none; border-left: none; border-right: none;">
		<?php echo trim(substr($pph21['no_bukti_potong'],0,4))." <u>".trim(substr($pph21['no_bukti_potong'],-13,2))."</u> ".trim(substr($pph21['no_bukti_potong'],6,1))." <u>".trim(substr($pph21['no_bukti_potong'],7,2))."</u>".trim(substr($pph21['no_bukti_potong'],9,1))." <u>".substr(trim($pph21['no_bukti_potong']),11)."</u>" ?>
        <?php //echo $pph21['no_bukti_potong'];?>
      </td>
	  <td width="78" style="text-align: center; font-weight: bold; font-size: 14px;  border-bottom: none; border-left: none; border-right: none;">&nbsp;</td>
	  <td width="65" style="text-align: center; font-weight: bold; font-size: 14px;  border-bottom: none; border-left: none; ">&nbsp;</td>
  </tr>
<tr>
	<td colspan="8"><hr/></td>
</tr>
</table>

<font style="font-family: arial; font-size: 11px;font-weight: bold;">A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</font>

<table border="1" class="" width="100%" style="font-weight: bold">
    <tr>
        <td width="1" height="41" style= "border-bottom: none; font-weight: bold;  border-right: none; border-left: none; border-top: none;">&nbsp;
            
        </td>
        <td width="109" style= "font-size: 14px; font-weight: bold; border-bottom: none; border-left: none; border-right: none; border-top: none;" >
            1.NPWP
        </td>
        <td width="10" style= "font-size: 14px;border-bottom: none; border-left: none; border-right: none;border-top: none;">
            :
        </td>
        <td width="20"  style= "font-size: 9.5px; border-bottom: none; border-left: none; border-right: none;border-top: none;"><font color="#57DDF9"; >A.01</font></td>
        <td colspan="4" style= " font-size: 14px; border-left: none; border-right: none;border-top: none; " >
		<?php echo trim(substr($pph21['npwp'],0,2)).".".trim(substr($pph21['npwp'],-2,3)).".".trim(substr($pph21['npwp'],-5,3)).".".trim(substr($pph21['npwp'],-8,1)) ?>
		</td>
        <td width="5" style= " font-size: 14px; border-left: none; text-align:center; border-right: none;border-top: none; border-bottom: none " ><b>-</b></td>
        <td width="17" style= " font-size: 14px; border-left: none; border-right: none;border-top: none;  " ><?php echo trim(substr($pph21['npwp'],9,3)) ?></td>
        <td width="4" style= " font-size: 14px; border-left: none; text-align:center; border-right: none;border-top: none;border-bottom: none; " ><b>.</b></td>
        <td style= "font-size: 14px; border-left: none; border-right: none;border-top: none; " ><?php echo trim(substr($pph21['npwp'],-12,3)) ?></td>
        <td width="240" style= "font-size: 14px;border-bottom: none; border-left: none; border-right: none; border-top: none;" >2. NIK / NO.PASPOR</td>
        <td width="6" style= " font-size: 14px;border-bottom: none; border-left: none; border-right: none;border-top: none;" >:</td>
        <td width="18" style= "font-size: 9px; border-bottom: none; border-left: none; border-right: none;border-top: none;" ><font color="#57DDF9";  >A.02</font></td>
        <td style= " font-size: 14px; border-left: none; border-right: none;border-top: none;" >
        <?=$pph21['nik'];?></td>
        <td style= " font-size: 14px; border-left: none; border-right: none;border-top: none; border-bottom: none" >&nbsp;</td>
    </tr>
    <tr>
        <td width="1" height="38" style= " font-size: 14px;border-bottom: none; border-right: none; border-top: none; border-left: none;">&nbsp;
            
      </td>
        <td width="79" style= " font-size: 14px; font-weight: bold; border-bottom: none; border-left: none; border-right: none; border-top: none;">
            3.NAMA</td>
        <td width="10" style= "font-size: 14px; border-bottom: none; border-left: none; border-right: none; border-top: none;">
            :
        </td>
        <td style= "font-size: 9px; border-bottom: none; border-left: none; border-right: none; border-top: none;"><font color="#57DDF9"; >A.03</font></td>
        <td colspan="12" style= " font-size: 14px; border-left: none;  border-top: none;border-right: none;"><?php echo $pph21['nama']; ?></td>
        <td style= " font-size: 14px; border-left: none;  border-top: none;border-right: none; border-bottom: none">&nbsp;</td>
    </tr>
    <tr>
      <td height="33" style= "font-size: 14px; border-bottom: none;  border-right: none; border-top: none; border-left: none;">&nbsp;</td>
      <td style= "font-size: 14px; border-bottom: none; border-left: none; border-right: none; border-top: none;" > 4.ALAMAT </td>
      <td style= "font-size: 14px; border-bottom: none; border-left: none; border-right: none; border-top: none;"> : </td>
      <td style= "font-size: 9px; border-bottom: none; border-left: none; border-right: none; border-top: none;"><font color="#57DDF9"; >A.04</font></td>
      <td colspan="12" style= "font-size: 14px; border-right: none; border-left: none;  border-top: none;">
        <?php
                echo $pph21['alamat'];
            ?>
      </td>
      <td style= "border-right: none; border-left: none;  border-top: none; border-bottom: none">&nbsp;</td>
    </tr>
    <tr>
      <td height="32" style= "border-bottom: none; border-right: none; border-top: none;border-left: none;">&nbsp;</td>
      <td colspan="2" style= " border-left: none; border-right: none; border-top: none; border-bottom: none">&nbsp;</td>
      <td style= " border-left: none; border-bottom: none;  border-top: none; border-right: none;"></td>
      <td colspan="12" style= " border-left: none; border-right: none;  border-top: none;"></td>
      <td style= " border-left: none; border-right: none;  border-top: none; border-bottom: none"></td>
    </tr>
    <tr>
        <td width="1" height="53" style= "  border-right: none; border-top: none; border-left: none; border-bottom:none;  ">&nbsp;
            
      </td>
        <td colspan="3" style= " font-size: 14px; border-left: none; border-right: none; border-top: none; border-bottom:none; border-bottom:none;">5.WAJIB PAJAK LUAR NEGRI<font color="#57DDF9"; size="0.25px">&nbsp;</font></td>
        <td width="55" style= " border-left: none; border-right: none; border-top: none; border-bottom:none;">&nbsp;</td>
        <td width="6" style= "font-size: 14px; border-left: none; border-right: none; border-top: none; border-bottom:none;">:</td>
        <td width="18" style= "font-size: 9px; border-left: none; border-right: none; border-top: none; border-bottom:none;"><font color="#57DDF9"; size="0.25px">A.05</font></td>
        <td width="68" style= " border-left: none; border-right: none; border-top: none; border-bottom:none;"><ins>
          <table width="113%" bordercolor=#000000>
            <tr>
              <td style="height:30px;padding: 1px; "><img src="<?=base_url('assets/dist/img/kotak1.png');?>" style="height:30px;" /> Ya</td>
            </tr>
          </table>
          
        </ins></td>
        <td style= " border-left: none; border-right: none; border-top: none; border-bottom:none;">&nbsp;</td>
        <td colspan="2" style= " border-left: none; border-right: none; border-top: none; border-bottom:none;">&nbsp;</td>
        <td width="135" style= " border-left: none; border-right: none; border-top: none; border-bottom:none;">&nbsp;</td>
        <td style= "font-size: 14px; border-left: none; border-right: none; border-top: none; border-bottom: none;">6. KODE NEGARA DOMISILI</td>
        <td style= "font-size: 14px; border-left: none; border-right: none; border-top: none; border-bottom: none;">:</td>
        <td style= " font-size: 9px; border-left: none; border-right: none; border-top: none; border-bottom: none;">
			<font color="#57DDF9"; size="0.25px" >A.06</font></td>
        <td width="305" style= " border-left: none;  border-top: none; border-right: none; ">&nbsp;</td>
        <td width="8" style= " border-left: none;  border-top: none; border-bottom: none; border-right: none">&nbsp;</td>
    </tr>
</table>

<font style="font-family: arial; font-size: 11px;font-weight: bold;">B. PPh PASAL 21 DAN/ATAU PASAL 26 YANG DIPOTONG</font>
<table border="1" style="border-collapse: collapse;margin-top:10px;" width="100%">
    <tr >
        <td style="font-weight: bold;text-align:center;font-size: 11px;width:170px;">
            KODE OBJEK PAJAK
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 11px;">
            JUMLAH<br/>
			PENGHASILAN BUTO <br/>
			(Rp)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 11px;width:170px;">
            DASAR PENGENAAN<br/>
			PAJAK <br/>
			(Rp)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 11px;width:120px;">
            Tarif Lebih Tinggi
            <br>
            100% (Tdk ber-NPWP)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 11px;width:70px;">
            Tarif
            <br>
            %
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 11px;width:170px;">
            PPh yang Dipotong
            <br>
            (Rp)
        </td>
  </tr>
    <tr style="background: #dadada;">
        <td height="28" style="font-weight: bold;text-align:center;font-size: 12px;">(1)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(2)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px; ">(3)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px; border=1">(4)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(5)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(6)</td>
    </tr>
    <tr style="background: #fff;">
        <td height="36" valign="top" style="font-weight: normal;text-align:center;font-size: 14px;">
			<?=$pph21['kode_pajak'];?></td>
        <td style="font-weight: normal;text-align:right;font-size: 14px;" valign="top">
	  <?=rupiah($pph21['jumlah_bruto']);?> </td>
        <td style="font-weight: normal;text-align:right;font-size: 14px;" valign="top"><?=rupiah($pph21['jumlah_dpp']);?></td>
        <td style="font-weight: normal;text-align:center;font-size: 14px;" valign="top">
			<?php
			if (trim($pph21['tanpa_npwp']=='Y'))
			{?>
				<img src="<?=base_url('assets/dist/img/kotak2.png');?>" style="height:30px;" />
			<?php }
			else {?>
		<img src="<?=base_url('assets/dist/img/kotak1.png');?>" style="height:30px;" />
			<?php }?>
		</td>
        <td style="text-align:center;font-size: 14px;" valign="top"><?=($pph21['tarif']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 14px;" valign="top"><?=rupiah($pph21['jumlah_pph']);?></td>
    </tr>
</table>

<font style="font-family: arial; font-size: 11px;font-weight: bold;">C. IDENTITAS PEMOTONG</font>

<table border="1" style="border-collapse: collapse;margin-top:10px;" width="100%">
	<tr>
		<td width="106" height="33" style="font-weight: bold;text-align:center;font-size: 14px;border-bottom:none; border-right: none">
			1.NPWP
		</td>
		<td width="13" style="font-weight: bold;text-align:center;font-size: 14px;border-bottom:none; border-left: none;border-right: none">
		:
		</td>
		<td width="40" style="font-weight: bold;text-align:center;font-size: 9px;border-bottom:none; border-left: none;border-right: none">
		<font color="#57DDF9"; size="0.25px">C.01</font>
		</td>
		<td width="133" style="font-weight: bold;text-align:left;font-size: 14px;border-left: none;border-right: none"><span style=" border-left: none; border-right: none;border-top: none; "><?php echo trim(substr($pph21['npwp_pemotong'],0,2)).".".trim(substr($pph21['npwp_pemotong'],2,3)).".".trim(substr($pph21['npwp_pemotong'],5,3)).".".trim(substr($pph21['npwp_pemotong'],8,1)) ?></span></td>
	  <td width="18" style="font-weight: bold;text-align:center;font-size: 14px;border-left: none;border-right: none;border-bottom: none;"><b>-</b></td>
		<td width="37" style="font-weight: bold;text-align:left;font-size: 14px;border-left: none;border-right: none"><span style=" border-left: none; border-right: none;border-top: none;  "><?php echo trim(substr($pph21['npwp_pemotong'],9,3)) ?></span></td>
		<td width="19" style="font-weight: bold;text-align:center;font-size: 14px;border-left: none;border-right: none;border-bottom: none;"><b>.</b></td>
		<td width="48" style="font-weight: bold;text-align:left;font-size: 14px;border-left: none;border-right: none"><span style=" border-left: none; border-right: none;border-top: none; "><?php echo trim(substr($pph21['npwp_pemotong'],-12,3)) ?></span></td>
		<td width="24" style="font-weight: bold;text-align:center;font-size: 14px;border-bottom:none;border-left: none;border-right: none">&nbsp;</td>
		<td colspan="5" style="font-weight: bold;text-align:left;font-size: 14px;border-bottom:none; border-left: none;">3. TANGGAL & TANDA TANGAN </td>
		<td width="254" rowspan="3" style="width:170px; border-left: none; ">
			
	<img src="<?=base_url('assets/dist/img/ttd-iwan1.png');?>" width="251" height="101"  />
		</td>
	</tr>
	<tr>
	  <td height="30" style="font-weight: bold;text-align:center;font-size: 14px;border-bottom:none; border-top: none; border-right: none"> 2.NAMA </td>
	  <td style="font-weight: bold;text-align:center;border-top:none; border-left: none; border-right: none; border-bottom: none; font-size: 14px;"> : </td>
	  <td style=" font-size: 9px; font-weight: normal; text-align:center; border-top:none; border-left: none; border-right: none; border-bottom: none; "><font color="#57DDF9"; size="0.25px" >C.02</font></td>
	  <td colspan="5" style="font-weight: bold;text-align:left;font-size: 14px;border-top:none; border-left: none; border-right: none">
	    <?=$pph21['nama_pemotong'];?>
	    </td>
	  <td width="24"  style= " font-size: 9px;border-bottom: none; border-left: none; border-right: none;border-top: none;">		<font color="#57DDF9"; size="0.25px">C.03</font>
	  </td>
	  <td width="98" style="font-weight: bold;text-align:center;font-size: 14px;border-top:none; border-left: none;border-right: none"><?php echo date('d');?></td>
	  <td width="18" style="font-weight: bold;text-align:center;font-size: 14px;border-top:none; border-left: none;border-right: none;border-bottom: none;">-</td>
	  <td width="29" style="font-weight: bold;text-align:center;font-size: 14px;border-top:none; border-left: none;border-right: none"><?php echo date('m');?></td>
	  <td width="18" style="font-weight: bold;text-align:center;font-size: 14px;border-top:none; border-left: none;border-bottom: none; border-right: none">-</td>
	  <td width="73" style="font-weight: bold;text-align:center;font-size: 14px;border-top:none; border-left: none;"><?php echo date('Y');?></td>
  </tr>
	<tr>
		<td height="31" style="font-weight: bold;text-align:center;font-size: 12px;border-top:none; border-right: none">&nbsp;</td>
		<td style="font-weight: bold;text-align:center;font-size: 12px;border-top:none; border-left: none; border-right: none">&nbsp;</td>
		<td style="font-weight: bold; text-align:center; border-top:none; border-left: none; border-right: none; ">&nbsp;</td>
		<td colspan="5" style="font-weight: bold;text-align:center;font-size: 12px;border-top:none; border-left: none; border-right: none">&nbsp;</td>
		<td style="font-weight: bold;text-align:center;border-top:none; border-left: none; border-right: none;">&nbsp;</td>
		<td colspan="5" style="font-weight: bold;text-align:left;font-size: 9px;border-top:none; border-left: none;">[dd-mm-yyyy]</td>
	</tr>
</table>


<table width="100%" border="1" style="border-collapse: collapse;margin-top:10px;">
  <tr style="background: #dadada;">
    <td colspan="3" style="font-weight: bold;text-align:center;font-size: 12px;">KODE OBJEK PAJAK PENGHASILAN PASAL 21 (TIDAK FINAL)ATAU PASAL 26</td>
  </tr>
  <tr style="background: #fff;">
    <td colspan="2" valign="top" style="font-weight: bold; text-align:center; border-right: none;    border-bottom: none;">&nbsp;</td>
    <td width="95%" valign="top" style="font-weight: bold;text-align:left;font-size: 11px; border-left: none; border-bottom: none;">PPh PASAL 21 TIDAK FINAL</td>
  </tr>
  <tr style="background: #fff;">
	  <td width="2%" valign="top" style="font-weight: bold;text-align:left;font-size: 12px;border-bottom: none;border-right: none; border-top: none" ><br/>
	  </td>
	  <td width="3%" valign="top" style="font-weight: normal;text-align:left;font-size: 10px; border-bottom: none;border-right: none; border-top: none; border-left: none; font-size: 10px;">1  .<br/>
	  	2.  <br/>
		3  .<br/>
		4.  <br/>
		5. <br/>
		6.  <br/>
		7.  <br/>
		8.  <br/>
		9.  <br/>
		10. <br/>
		11. <br/>
		12. <br/></td>
    <td colspan="2" valign="top" style="font-weight: normal;text-align:left;font-size: 10px; border-bottom:none; border-top: none; border-left: none;font-size: 10px">
		 21-100-03 Upah Pegawai Tidak Tetap atau Kerja Lepas <br/>
	  	  21-100-04 Upah Pegawai Tidak Tetap atau Tenaga Kerja Lepas <br/>
		  21-100-05 Imbalan Kpada Distributor Multi Level Marketing (MLM) <br/>
		  21-100-06 Imbalan Kepada Petugas Dinas Luar Asuransi <br/>
		  21-100-07 Imbalan Kepada Panjaja Barang Dagangan <br/>
		 21-100-08 Imbalan Kepada Tenaga Ahli <br/>
		 21-100-09 Imbalan Kepada Bukan Pegawai yang Menerima Penghasilan yang Bersifat Berkesinambungan <br/>
		 21-100-10 Imbalan Kepada Bukan Pegawai yang Menerima Penghasilan yang Tidak Bersifat Berkesinambungan <br/>
		  21-100-11 Honorarium atau Imbalan Kepada Anggota Dewan Komisaris atau Dewan Pengawas yang tidak Merangkap sebagai Pegawai Tetap <br/>
		21-100-12 Jasa Produksi, Tantiem .Bonus atau Imbalan Kepada Mantan Pegawai <br/>
		 21-100-13 Penarikan Dana Pensiun oleh Pegawai <br/>
		 21-100-99 Imbalan Kepada Peserta Kegiatan 
	  </td>
  </tr>
  <tr style="background: #fff;">
    <td colspan="2" valign="top" style="font-weight: bold;text-align:center;font-size: 12px;border-bottom: none;border-top:none;border-right: none;">&nbsp;</td>
    <td valign="top" style="font-weight: bold;text-align:left;font-size: 11px; border-bottom:none; border-top:none;border-left: none">PPH PASAL 26</td>
  </tr>
  <tr >
    <td valign="top" style="font-weight: bold;text-align:left;font-size: 12px; border-top:none;border-right: none;" ></td>
    <td valign="top" style="font-weight: normal;text-align:left;font-size: 12px;border-top:none;border-right: none; border-left: none;font-size: 10px" >1. </td>
    <td valign="top" style="font-weight: normal;text-align:left;font-size: 12px; border-top:none; border-left: none; font-size: 10px" >27-100-99 Imbalan sehubungan dengan jaa, pekerjaan dan kegiatan, hadiah dan penghargaan, pensiun dan pembayaran berkala lainnya yang
	dipotong PPh Pasal 26</td>
  </tr>
</table>
<p>&nbsp;</p>