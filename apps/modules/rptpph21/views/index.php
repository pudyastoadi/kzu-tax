<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-body">
                <?php
                    $attributes = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => 'blank'
                        , 'class' => "form-inline");
                    echo form_open($submit,$attributes); 
                ?> 
                    <div class="form-group">
                        <input type="hidden" name="id" id="id" class="form-control" />
                        <?php 
                            echo form_label($form['periode_awal']['placeholder']);
                            echo form_input($form['periode_awal']);
                            echo form_error('periode_awal','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <button type="button" class="btn btn-primary btn-tampil">Tampil</button>
<!--                    <button type="submit" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Cetak Bukti Potong PPh</button>-->
                <?php echo form_close(); ?>
                <hr>
                
                <?php
                    $attributes_2 = array(
                        'role=' => 'form'
                        , 'id' => 'form_add'
                        , 'name' => 'form_add'
                        , 'target' => 'blank'
                        , 'class' => "form-inline");
                    echo form_open("#",$attributes_2); 
                ?> 
                <div class="form-group">
                    <?php 
                        echo form_label($form['nama_kpp']['placeholder']);
                        echo form_input($form['nama_kpp']);
                        echo form_error('nama_kpp','<div class="note">','</div>'); 
                    ?>
                </div>
                <div class="form-group">
                    <?php 
                        echo form_label($form['lokasi_kpp']['placeholder']);
                        echo form_input($form['lokasi_kpp']);
                        echo form_error('lokasi_kpp','<div class="note">','</div>'); 
                    ?>
                </div>
                <?php echo form_close(); ?>
                <hr>
                
                <div class="table-responsive">
                <table  class="table table-bordered table-hover js-basic-example dataTable">
                    <thead>
                        <tr>
                            <th style="width:10px; text-align: center;">TGL UPLOAD</th>
                            <th style="text-align: center;">NO. PPh</th>
                            <th style="width:10px; text-align: center;">NPWP</th>
                            <th style="text-align: center;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="width:10px; text-align: center;">TGL BUKTI POTONG</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th style="text-align: center;">TGL UPLOAD</th>
                            <th style="text-align: center;">NO. PPh</th>
                            <th style="text-align: center;">NPWP</th>
                            <th style="text-align: center;">NAMA</th>
                            <th style="text-align: center;">ALAMAT</th>
                            <th style="text-align: center;">TGL BUKTI POTONG</th>
                        </tr>                      
                    </tfoot>
                    <tbody></tbody>
                </table>
                </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        if(localStorage.nama_kpp){
            $('#nama_kpp').val(localStorage.nama_kpp);
        }
        if(localStorage.lokasi_kpp){
            $('#lokasi_kpp').val(localStorage.lokasi_kpp);
        }
        $(".btn-tampil").click(function(){
            table.ajax.reload();
        });
        table = $('.dataTable').DataTable({
            "lengthMenu": [[100,200,400,-1], [100,200,400,"Semua Data"]],
            "bProcessing": true,
            "bServerSide": true,
            "bDestroy": true,
            "bAutoWidth": false,
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                aoData.push( { "name": "periode_awal", "value": $("#periode_awal").val() }
                            ,{ "name": "periode_akhir", "value": $("#periode_akhir").val() });
                $.ajax( {
                    "dataType": 'json', 
                    "type": "GET", 
                    "url": sSource, 
                    "data": aoData, 
                    "success": fnCallback
                } );
            },
            'rowCallback': function(row, data, index){
                //if(data[21]){
                    //$(row).find('td:eq(21)').css('background-color', '#ff9933');
                //}
            },
            "sAjaxSource": "<?=site_url('rptpph21/json_dgview');?>",
            "oLanguage": {
                "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan tunggu...",
                buttons: {
                    selectAll: "Pilih Semua",
                    selectNone: "Batalkan Pilihan"
                }                
            },
            //dom: '<"html5buttons"B>lTfgitp',
            select: true,
            buttons: [
                'selectAll',
                'selectNone',
                {
                    text: '<i class="fa fa-file-pdf-o"></i> Export PDF',
                    action: function () {
                        if($(".btn").is(':disabled')) { 
                            alert('No need to click, it\'s disabled.');
                        }else{
                            var data = table.rows( { selected: true } ).data();
                            exportpdf(data);
                        }
                    }
                },
				{
                    text: '<i class="fa fa-trash"></i> Delete Yang Dipilih',
                    action: function () {
                        if($(".btn").is(':disabled')) { 
                            alert('No need to click, it\'s disabled.');
                        }else{
                            var data = table.rows( { selected: true } ).data();
                            deleteItem(data);
                        }
                    }
                }
            ],
            "sDom": "<'row'<'col-sm-6'l i><'col-sm-6 text-right'>B r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> "
        });
        
        $('.dataTable').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });

        $('.dataTable tfoot th').each( function () {
            var title = $('.dataTable tfoot th').eq( $(this).index() ).text();
            if(title!=="Edit" && title!=="Delete" && title!=="AKSI" ){
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;border-radius: 0px;" placeholder="Search" />' );
            }else{
                $(this).html( '' );
            }
        } );

        table.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function (ev) {
                //if (ev.keyCode == 13) { //only on enter keypress (code 13)
                    that
                        .search( this.value )
                        .draw();
                //}
            } );
        });
        
        table.on( 'select', function ( e, dt, type, indexes ) {
            if ( type === 'row' ) {
                var data = table.rows( { selected: true } ).data();
                var id = "";
                $.each(data, function(key, data){
                    if(id===""){
                        id = data[6];
                    }else{
                        id += "," + data[6];
                    }
                    //id.push(data[6]);
                });
                $("#id").val(id);
            }
        } );
        
        table.on( 'deselect', function ( e, dt, type, indexes ) {
            if ( type === 'row' ) {
                var data = table.rows( { selected: true } ).data();
                var id = "";
                $.each(data, function(key, data){
                    if(id===""){
                        id = data[6];
                    }else{
                        id += "," + data[6];
                    }
                    //id.push(data[6]);
                });
                $("#id").val(id);
            }
        } );
    });
    
    function exportpdf(param){ 
        //console.log(param);
        var id = [];
        $.each(param, function(key, data){
            id.push(data[6]);
        });
        var nama_kpp = $('#nama_kpp').val();
        var lokasi_kpp = $('#lokasi_kpp').val();
        if(nama_kpp.length===0){
            swal({
                title: "Peringatan",
                text: "Nama KPP tidak boleh kosong",
                type: "error"
            });
            return;
        }if(lokasi_kpp.length===0){
            swal({
                title: "Peringatan",
                text: "Lokasi KPP tidak boleh kosong",
                type: "error"
            });
            return;
        }
        if(id.length>0){
            localStorage.nama_kpp = nama_kpp;
            localStorage.lokasi_kpp = lokasi_kpp;
            $.ajax({
                type: "POST",
                url: "<?=site_url('rptpph21/submit');?>",
                data: {id
                        ,"nama_kpp":nama_kpp
                        ,"lokasi_kpp":lokasi_kpp},
                beforeSend: function(resp){
                    $('.btn').attr('disabled',true);
                },
                success: function(resp){  
                    $('.btn').attr('disabled',false);
                    console.log(resp);
                    swal({
                        title: "File berhasil dibuat",
                        text: "Mendownload file",
                        type: "info"
                    }, function () {
//                        window.location.href = '<?=base_url('files/');?>'+resp;
                        window.open('<?=base_url('files/');?>'+resp);
                    });
                },
                error:function(event, textStatus, errorThrown) {
                    $('.btn').attr('disabled',false);
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
            });
        }else{
            swal({
                title: "Peringatan",
                text: "Silahkan pilih data yang akan dicetak",
                type: "error"
            }, function () {
                
            });
        }
    }
	
	function deleteItem(param){
		var id = [];
        $.each(param, function(key, data){
            id.push(data[6]);
        });
		
		$.ajax({
			type:"POST",
			url : "<?=site_url('rptpph21/delete');?>",
			data:{id},
			beforeSend: function(resp){
                    $('.btn').attr('disabled',false);
                },
			success:function(resp){
                    console.log(resp);
                    swal({
                        title: "Item Berhasil Dihapus",
                        text: "Menghapus File",
                        type: "info"
                    }, function () {
					location.reload();
				});
			},error:function(event, textStatus, errorThrown) {
                    $('.btn').attr('disabled',false);
                    console.log("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
                }
			
		});
	}
</script>