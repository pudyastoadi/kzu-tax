<style> 
    .code {
        background-color: #dadada; 
        /*box-shadow: 5px 4px #000;*/
        margin-left: 23%;
        margin-right: 20%;
        margin-bottom: 10px;
    }

    font.border{
        border:1px solid #000; 
    }

    font.no-border{
        width: 10px;
    }

    span.desc{
        font-weight: normal;
        font-size: 8px;
    }
    td{
        font-family: 'FreeSans';
    }
</style>
<table border="0" class="" width="100%">
    <tr>
        <td colspan="4">
            <table width="100%">
                <tr>
                    <td style="background-color: #000;height:12px;padding: 0px;">
                        
                    </td>
                    <td style="width:90%;padding: 0px;">

                    </td>
                    <td style="background-color: #000;padding: 0px;">
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td style="text-align: center;font-weight: bold;">
            
        </td>
        <td width="60px;">
            
        </td>
        <td valign="top" width="230px;">
            <table style="font-weight: bold; font-size: 10px;">
                <tr>
                    <td>
                        Lembar ke-1 untuk : yang menyewakan<br>
                        Lembar ke-2 untuk : Kantor Pelayanan Pajak<br>
                        Lembar ke-3 untuk : penyewa
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <img src="<?=base_url('assets/dist/img/logo-tax-mini.jpg');?>" style="height:75px;" />
        </td>
        <td style="text-align: center;font-weight: bold;font-size: 12.5px;">
            DEPARTEMEN KEUANGAN REPUBLIK INDONESIA
            <br>
            DIREKTORAT JENDRAL PAJAK
            <br>
            KANTOR PELAYANAN PAJAK
            <br>
            <?=strtoupper($nama_kpp);?>
        </td>
        <td>
            
        </td>
        <td valign="top">
            
        </td>
    </tr>
</table>
<br><br>
<div class="code">
    <table border="1" width="100%" style="border-collapse: collapse;">
        <tr>
            <td style="text-align: center;background: #dadada; font-weight: bold;height: 30px;font-size: 12.5px;">
                BUKTI PEMOTONGAN PPh FINAL PASAL 4 AYAT (2)
                <br>
                ATAS PENGHASILAN DARI PERSEWAAN TANAH
                <br>
                DAN/ATAU BANGUNAN
            </td>
        </tr>
        <tr>
            <td style="text-align: center;background: #dadada; font-weight: bold;padding: 5px;padding-bottom:8px;font-size: 12.5px;">
                Nomor : <?=$pph42['nb_dp'];?>
            </td>
        </tr>
    </table>
</div>

<br><br>
<table border="0" class="" width="100%">
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="140px;" style="font-weight: bold;">
            NPWP
        </td>
        <td width="20px;">
            :
        </td>
        <td style="padding: 0px;">
            <?php echo format_npwp_2($pph42['npwp_wp_dp']); ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="140px;" style="font-weight: bold;">
            Nama WP
        </td>
        <td width="20px;">
            :
        </td>
        <td style="padding: 0px;">
            <?php echo format_nmwp($pph42['nm_wp_dp']); ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="140px;" style="font-weight: bold;">
            Alamat
        </td>
        <td width="20px;">
            :
        </td>
        <td style="padding: 0px;">
            <?php
                echo format_nmwp($pph42['alamat_wp_dp']);
            ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="140px;" style="font-weight: bold;">
            Lokasi Tanah dan
        </td>
        <td width="20px;">
            :
        </td>
        <td style="padding: 0px;">
            <?php
                echo format_nmwp(substr($pph42['jenis_hadiah_1'], 0, 31));
            ?>
        </td>
    </tr>
    <tr>
        <td width="20px;">
            &nbsp;
        </td>
        <td width="140px;" style="font-weight: bold;">
            atau Bangunan
        </td>
        <td width="20px;">
            
        </td>
        <td style="padding: 0px;">
            <?php
                echo format_nmwp(substr($pph42['jenis_hadiah_1'], 32, 64));
            ?>
        </td>
    </tr>
</table>


<br><br>
<table border="1" style="border-collapse: collapse;margin-top:10px;" width="100%">
    <tr style="background: #dadada;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;">
            Jumlah Bruto Nilai Sewa
            <br>
            (Rp)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:70px;">
            Tarif
            <br>
            (%)
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">
            PPh yang Dipotong
            <br>
            (Rp)
        </td>
    </tr>
    <tr style="background: #dadada;">
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(1)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(2)</td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;">(3)</td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: normal;text-align:right;font-size: 12px;height: 30px;vertical-align: middle;padding-right: 10px;"><?=rupiah($pph42['jml_bruto_1']);?></td>
        <td style="text-align:center;font-size: 12px;vertical-align: middle;"><?=persen($pph42['trf_1']);?></td>
        <td style="font-weight: normal;text-align:right;font-size: 12px;vertical-align: middle;padding-right: 10px;"><?=rupiah($pph42['pph_dp_1']);?></td>
    </tr>
    <tr style="background: #fff;">
        <td style="font-weight: normal;text-align:left;font-size: 13px;border-top:none;height:30px;" colspan="3" valign="middle">
            Terbilang: &nbsp;&nbsp; <?=ucwords(terbilang($pph42['pph_dp'])." Rupiah");?>
        </td>
    </tr>
</table>


<br><br>
<table border="0" style="border: none;margin-top:30px;" width="100%">
    <tr>
        <td style="font-weight: bold;text-align:left;font-size: 12px;">
            
        </td>
        <td style="font-weight: bold;text-align:center;font-size: 12px;width:490px;border: none;" valign="top">
            <?php
                $tgl = explode("-", $pph42['tg_dp']);
            ?>
            <?=strtoupper($lokasi_kpp);?>,  <?=$tgl[2]." ".month($tgl[1])." ".$tgl[0];?>
            <table width="100%" style="padding-top:10px;">
                <tr>
                    <td colspan="3" style="font-weight: bold;height: 30px;">Pemotong Pajak</td>
                </tr>  
                <tr>
                    <td style="text-align: left;width: 70px;font-weight: bold;">N P W P</td>
                    <td style="width: 1px;">:</td>
                    <td style="padding: 0px;"><?php echo format_npwp_2_footer('014460810511000');?></td>
                </tr>   
                <tr>
                    <td style="text-align: left;width: 70px;font-weight: bold;">N a m a</td>
                    <td style="width: 1px;">:</td>
                    <td style="padding: 0px;">
                        <?php 
                            echo format_nmwp('PT. KARYA ZIRANG UTAMA',19);
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td style="text-align: left;width: 70px;font-weight: bold;"></td>
                    <td style="width: 1px;"></td>
                    <td style="padding: 0px;">
                        <?php 
                            echo format_nmwp('MA',19);
                        ?>
                    </td>
                </tr>  
                <tr>
                    <td style="text-align: left;width: 70px;"></td>
                    <td style="width: 1px;"></td>
                    <td>
                    </td>
                </tr>               
            </table>
        </td>
    </tr>
</table>


<br><br>
<table border="1" width="100%" style="border-collapse: collapse;margin-top:10px;">
    <tr>
        <td style="font-weight: bold;text-align:left;font-size: 12px;width:36%;">
            <table>
                <tr>
                    <td colspan="2" style="font-weight: normal;font-style: italic;">    
                        Perhatian : 
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-weight: normal;">
                        1.
                    </td>
                    <td style="font-weight: normal;font-size: 13px;">
                        Jumlah Pajak Penghasilan atas 
                        Persewaan Tanah dan/ atau Bangunan
                        yang dipotong di atas bukan merupakan
                        kredit pajak dalam Surat Pemberitahuan
                        (SPT) Tahunan PPh.
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="font-weight: normal;">
                        2.
                    </td>
                    <td style="font-weight: normal;font-size: 13px;">
                        Bukti Pemotongan ini dianggap sah
                        apabila diisi dengan lengkap dan benar.
                    </td>
                </tr>
            </table>        
        </td>
        <td style="font-weight: bold;text-align:left;font-size: 12px;width:20%;">
            
        </td>
        <td style="font-weight: normal;text-align:center;font-size: 14px;" valign="top">
            <strong>Tanda Tangan, Nama dan Cap</strong>
            <br>
            <br>
            <img src="<?=base_url('assets/dist/img/ttd.png');?>" style="max-width: 250px;max-height: 110px;position: relative;left: 35px;"/>
            <br>
            <strong><u>NUR KHAYATI</u></strong>
            <br>
            <strong>ACCOUNTING MANAGER</strong>
        </td>
        <td style="font-weight: normal;text-align:center;font-size: 14px;">
            &nbsp;
        </td>
    </tr>
</table>