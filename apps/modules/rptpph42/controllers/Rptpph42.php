<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Rptpph42
 *
 * @author adi
 */
class Rptpph42 extends MY_Controller {
    protected $data = '';
    protected $val = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => $this->msg_main,
            'msg_detail' => $this->msg_detail,
            
            'submit' => site_url('rptpph42/submit'),
            'add' => site_url('rptpph42/add'),
            'edit' => site_url('rptpph42/edit'),
            'delete' => site_url('rptpph42/delete'),
            'reload' => site_url('rptpph42'),
        );
        $this->load->model('rptpph42_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){
        $this->_init_add();
        $this->template
            ->title($this->data['msg_main'],$this->apps->name)
            ->set_layout('main')
            ->build('index',$this->data);
    }
    
    public function submit() {  
        $this->load->library('zip');
        $data_array  = $this->rptpph42_qry->exportpdf();
//        foreach($data_array as $val){
//            $this->data['pph42'] = $val;
//            $this->load->view('rpt_pdf', $this->data);
//        }                      
        
        //load mPDF library
        
        ini_set("memory_limit","256M");
        ini_set('max_execution_time',360000);
        set_time_limit(360000) ;
        $this->load->library('M_pdf');
         
        $no = 0;
        array_map('unlink', glob( "./files/bukti-potong-pajak*"));
        $this->data['nama_kpp'] = $this->input->post('nama_kpp');
        $this->data['lokasi_kpp'] = $this->input->post('lokasi_kpp');
        foreach($data_array as $val){
            $pdf = $this->m_pdf->load();

            $pdf->SetTitle($val['nm_wp_dp']);
            $pdf->SetAuthor($this->apps->title);
            $pdf->SetCreator($this->apps->name);
            $pdf->SetSubject('Bukti Pemotongan PPh Pasal 23');
            $pdf->SetKeywords('Bukti Pemotongan PPh Pasal 23, PPh Pasal 23, Bukti Potong');
        
            //this the the PDF filename that user will get to download
            $pdfFilePath = strtolower(preg_replace("/[^A-Za-z0-9]/", '-',$val['nb_dp']))."-".strtolower(preg_replace("/[^A-Za-z0-9]/", '-',$val['nm_wp_dp'])).".pdf";  
        
            $this->data['pph42'] = $val;
            $pdf->AddPage('P','','','','',10, 10, 10, 10,0,10,'','','','','','','','','','Legal');  
            $pdf->SetHTMLFooter('<table border="0" class="" width="100%">
                                    <tr>
                                        <td style="padding-left: 10px;font-size: 12px;"><strong>F.1.1.33.12</strong></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td style="background-color: #000;height:12px;">

                                                    </td>
                                                    <td style="width:90%">

                                                    </td>
                                                    <td style="background-color: #000;">

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>');

            //generate the PDF!
            //$pdf->cacheTables = true;
            //$pdf->simpleTables=true;
            //$pdf->packTableData=true;
            $html=$this->load->view('rpt_pdf', $this->data,true);
            $pdf->WriteHTML($html);
            //$pdf->debug = false;
            //download it.
            $pdf->Output('./files/'.$pdfFilePath, "F");
            $this->zip->read_file('./files/'.$pdfFilePath);
            $no++;
        }
//        echo $pdfFilePath;
        // Download the file to your desktop. Name it "my_backup.zip"
        $this->zip->archive('./files/'.'bukti-potong-pajak-'.date('Y-m-d-H-i-s').'.zip');
        echo 'bukti-potong-pajak-'.date('Y-m-d-H-i-s').'.zip';
    }
    
    public function json_dgview() {
        echo $this->rptpph42_qry->json_dgview();
    }
	
	public function delete(){
		echo $this->rptpph42_qry->deleteSelect();	
	}
	
    private function _init_add(){
        $this->data['form'] = array(
           'periode_awal'=> array(
                    'placeholder' => 'Periode Upload',
                    'id'          => 'periode_awal',
                    'name'        => 'periode_awal',
                    'value'       => set_value('periode_awal'),
                    'class'       => 'form-control calendar',
                    'style'       => 'margin-left: 5px;',
            ),
           'nama_kpp'=> array(
                    'placeholder' => 'Nama KPP',
                    'id'          => 'nama_kpp',
                    'name'        => 'nama_kpp',
                    'value'       => set_value('nama_kpp'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
            ),
           'lokasi_kpp'=> array(
                    'placeholder' => 'Lokasi KPP',
                    'id'          => 'lokasi_kpp',
                    'name'        => 'lokasi_kpp',
                    'value'       => set_value('lokasi_kpp'),
                    'class'       => 'form-control',
                    'style'       => 'margin-left: 5px;',
            ),
        );
    }
}
