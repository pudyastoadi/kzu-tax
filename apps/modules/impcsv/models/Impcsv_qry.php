<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Impcsv_qry
 *
 * @author adi
 */
class Impcsv_qry extends CI_Model{
    //put your code here
    protected $res="";
    protected $delete="";
    protected $state="";
    protected $kd_cabang = "";
    public function __construct() {
        parent::__construct();  
        $this->kd_cabang = $this->apps->kd_cabang;
    }
    
    public function submit() {
        try {
            $this->load->library('CSVReader');
            if( isset( $_FILES['csvfile'] ) == TRUE && !empty($_FILES['csvfile']['name'])){
                //$files = array_filter($_FILES['upload']['name']); something like that to be used before processing files.

                // Loop through each file
                $rows = array();
                $sess_umdo = array();
                $this->db->trans_begin();
                  //Get the temp file path
                  $tmpFilePath = $_FILES['csvfile']['tmp_name'];
                  //Make sure we have a filepath
                  if ($tmpFilePath != ""){
                    //Setup our new file path
                    $newFilePath = './files/' . $_FILES['csvfile']['name'];
                    //Upload the file into the temp dir
                    if(move_uploaded_file($tmpFilePath, $newFilePath)) {
                        //Handle other code here
                        $filename = $_FILES['csvfile']['name'];
                        $csvData = $this->csvreader->parse_file($newFilePath,false); //path to csv file
                        $path_parts = pathinfo($newFilePath);
                        if($path_parts['extension']==="csv"){
                            // Jika dalam file ada tanda kurung maka dianggap file ganda dan tidak akan di proses
                            //var_dump($csvData);
                            if($csvData){
                                $no = 0;
                                foreach($csvData as $csv){
                                    if($no>0){
                                        if((strtoupper($csv[10])==strtoupper("21-100-09"))
											or (strtoupper($csv[10])==strtoupper("21-100-08"))){
                                            $rows = $this->pharseDataPph21($csv);
                                            if($rows){
                                                $res = $this->db->insert('tran_csv_pph_21',$rows);
                                                if($res==FALSE){
                                                    $this->db->trans_rollback();
                                                    $err = $this->db->error();
                                                    unlink($newFilePath);
                                                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                                }  
                                                $this->tipe = "PPh 21";
                                            }    
                                        } 
                                        elseif(strtoupper($csv[0])=="F113306"){
                                            $rows = $this->pharseDataPph23($csv);
                                            if($rows){
                                                $res = $this->db->insert('tran_csv_pph_23',$rows);
                                                if($res==FALSE){
                                                    $this->db->trans_rollback();
                                                    $err = $this->db->error();
                                                    unlink($newFilePath);
                                                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                                }  
                                                $this->tipe = "PPh Pasal 23";
                                            }    
                                        } 
                                        elseif(strtoupper($csv[0])=="F113312"){
                                            $rows = $this->pharseDataPph4_2($csv);
                                            if($rows){
                                                $res = $this->db->insert('tran_csv_pph_4_2',$rows);
                                                if($res==FALSE){
                                                    $this->db->trans_rollback();
                                                    $err = $this->db->error();
                                                    unlink($newFilePath);
                                                    throw new Exception(" Error : ". $this->apps->err_code($err['message']));
                                                }  
                                                $this->tipe = "PPh Pasal 4 ayat 2";
                                            }  
                                        }  
                                    }
//                                    else {
//                                        $this->db->trans_rollback();
//                                        $err = $this->db->error();
//                                        unlink($newFilePath);
//                                        throw new Exception(" Error : Kode Dokumen Tidak Dikenal : " . $csv[0]);
//                                        
//                                    }
                                    $no++;
                                }
                            }
                            else{
                                unlink($newFilePath);
                                throw new Exception(" Error : Isi File CSV Kosong, Silahkan Cek Kembali");
                            }    
                        }else{
                            unlink($newFilePath);
                            throw new Exception("Error : Tidak ada data yang di simpan, karena file salah atau gagal di simpan ke database"
                                    . " <br>Reff File : Wrong File!");

                        }
                    }
                    unlink($newFilePath);
                  }
                if($res!==FALSE){
                    $this->db->trans_commit();
                    $this->res = $this->tipe . " : Data Tersimpan";
                    $this->state = "1";
                }
            }else{
                $this->res = " Error : ". "Data file CSV masih kosong"
                                . " , transaksi dibatalkan."
                                . " Reff File : Empty File!";
                $this->state = "0";
            }
        }catch (Exception $e) {     
            $this->db->trans_rollback();
            $this->res = $e->getMessage();
            $this->state = "0";
        } 
        
        $arr = array(
            'state' => $this->state, 
            'msg' => $this->res,
            );
        $this->session->set_userdata('statsubmit', json_encode($arr));
        return $arr;
    }
    
    private function pharseDataPph23($value) {  
        $data['kfbp'] = $value[0];
        $data['masa_pajak'] = $value[1];
        $data['tahun_pajak'] = $value[2];
        $data['pembetulan'] = $value[3];
        $data['npwp_wp_dp'] = $value[4];
        $data['nm_wp_dp'] = $value[5];
        $data['alamat_wp_dp'] = $value[6];
        $data['nb_dp'] = $value[7];
        $data['tg_dp'] = $this->apps->dateConvert(str_replace('/','-',$value[8]));
        $data['nb_1'] = $value[9];
        $data['trf_1'] = $value[10];
        $data['pph_dp_1'] = $value[11];
        $data['nb_2'] = $value[12];
        $data['trf_2'] = $value[13];
        $data['pph_dp_2'] = $value[14];
        $data['nb_3'] = $value[15];
        $data['trf_3'] = $value[16];
        $data['pph_dp_3'] = $value[17];
        $data['nb_4'] = $value[18];
        $data['trf_4'] = $value[19];
        $data['pph_dp_4'] = $value[20];
        $data['nb_5'] = $value[21];
        $data['trf_5'] = $value[22];
        $data['pph_dp_5'] = $value[23];
        $data['nb_6a'] = $value[24];
        $data['trf_6a'] = $value[25];
        $data['pph_dp_6a'] = $value[26];
        $data['nb_6b'] = $value[27];
        $data['trf_6b'] = $value[28];
        $data['pph_dp_6b'] = $value[29];
        $data['nb_6c'] = $value[30];
        $data['trf_6c'] = $value[31];
        $data['pph_dp_6c'] = $value[32];
        $data['nb_9'] = $value[33];
        $data['trf_9'] = $value[34];
        $data['pph_dp_9'] = $value[35];
        $data['nb_10'] = $value[36];
        $data['pp_netto_10'] = $value[37];
        $data['trf_10'] = $value[38];
        $data['pph_dp_10'] = $value[39];
        $data['nb_11'] = $value[40];
        $data['pp_netto_11'] = $value[41];
        $data['trf_11'] = $value[42];
        $data['pph_dp_11'] = $value[43];
        $data['nb_12'] = $value[44];
        $data['pp_netto_12'] = $value[45];
        $data['trf_12'] = $value[46];
        $data['pph_dp_12'] = $value[47];
        $data['nb_13'] = $value[48];
        $data['trf_13'] = $value[49];
        $data['pph_dp_13'] = $value[50];
        $data['kj_6d1'] = $value[51];
        $data['nb_6d1'] = $value[52];
        $data['trf_6d1'] = $value[53];
        $data['pph_dp_6d1'] = $value[54];
        $data['kj_6d2'] = $value[55];
        $data['nb_6d2'] = $value[56];
        $data['trf_6d2'] = $value[57];
        $data['pph_dp_6d2'] = $value[58];
        $data['kj_6d3'] = $value[59];
        $data['nb_6d3'] = $value[60];
        $data['trf_6d3'] = $value[61];
        $data['pph_dp_6d3'] = $value[62];
        $data['kj_6d4'] = $value[63];
        $data['nb_6d4'] = $value[64];
        $data['trf_6d4'] = $value[65];
        $data['pph_dp_6d4'] = $value[66];
        $data['kj_6d5'] = $value[67];
        $data['nb_6d5'] = $value[68];
        $data['trf_6d5'] = $value[69];
        $data['pph_dp_6d5'] = $value[70];
        $data['kj_6d6'] = $value[71];
        $data['nb_6d6'] = $value[72];
        $data['trf_6d6'] = $value[73];
        $data['pph_dp_6d6'] = $value[74];
        $data['jml_bruto'] = $value[75];
        $data['pph_dp'] = $value[76];
        $query = $this->db->get_where('tran_csv_pph_23', $data);
        if($query->num_rows()>0){
            return false;
        }else{
            return $data;   
        }         
    }
    
    private function pharseDataPph4_2($value) {
        $data['kfbp'] = $value[0];
        $data['masa_pajak'] = $value[1];
        $data['tahun_pajak'] = $value[2];
        $data['pembetulan'] = $value[3];
        $data['npwp_wp_dp'] = $value[4];
        $data['nm_wp_dp'] = $value[5];
        $data['alamat_wp_dp'] = $value[6];
        $data['nb_dp'] = $value[7];
        $data['tg_dp'] = $this->apps->dateConvert(str_replace('/','-',$value[8]));
        
        $data['jenis_hadiah_1'] = $value[9];
        $data['kd_opt_1'] = $value[10];
        $data['jml_bruto_1'] = $value[11];
        $data['trf_1'] = $value[12];
        $data['pph_dp_1'] = $value[13];
        
        $data['jenis_hadiah_2'] = $value[14];
        $data['kd_opt_2'] = $value[15];
        $data['jml_bruto_2'] = $value[16];
        $data['trf_2'] = $value[17];
        $data['pph_dp_2'] = $value[18];
        
        $data['jenis_hadiah_3'] = $value[19];
        $data['kd_opt_3'] = $value[20];
        $data['jml_bruto_3'] = $value[21];
        $data['trf_3'] = $value[22];
        $data['pph_dp_3'] = $value[23];
        
        $data['jenis_hadiah_4'] = $value[24];
        $data['kd_opt_4'] = $value[25];
        $data['jml_bruto_4'] = $value[26];
        $data['trf_4'] = $value[27];
        $data['pph_dp_4'] = $value[28];
        
        $data['jenis_hadiah_5'] = $value[29];
        $data['kd_opt_5'] = $value[30];
        $data['jml_bruto_5'] = $value[31];
        $data['trf_5'] = $value[32];
        $data['pph_dp_5'] = $value[33];
        
        $data['jenis_hadiah_6'] = $value[34];
        $data['jml_bruto_6'] = $value[35];
        $data['trf_6'] = $value[36];
        $data['pph_dp_6'] = $value[37];
        
        $data['jml_bruto_7'] = $value[38];
        $data['trf_7'] = $value[39];
        $data['pph_dp_7'] = $value[40];
        
        $data['jp_8'] = $value[41];
        $data['jml_bruto_8'] = $value[42];
        $data['trf_8'] = $value[43];
        $data['pph_dp_8'] = $value[44];
        
        $data['pph_dp'] = $value[45];
        $data['tg_tempo'] = $this->apps->dateConvert(str_replace('/','-',$value[46]));
        $data['tg_trm_obligasi'] = $this->apps->dateConvert(str_replace('/','-',$value[47]));
        $data['tg_jual_obligasi'] = $this->apps->dateConvert(str_replace('/','-',$value[48]));
        $data['hold_obligasi'] = $value[49];
        $data['time_obligasi'] = $value[50];
        
        $query = $this->db->get_where('tran_csv_pph_4_2', $data);
        if($query->num_rows()>0){
            return false;
        }else{
            return $data;   
        }         
    }

    private function pharseDataPph21($value) {
        
        $data['masa_pajak'] = $value[0];
        $data['thn_pajak'] = $value[1];
        $data['pembetulan'] = $value[2];
        $data['no_bukti_potong'] = $value[3];
        $data['npwp'] = $value[4];
        $data['nik'] = $value[5];
        $data['nama'] = $value[6];
        $data['alamat'] = $value[7];
        $data['wp_luar_negeri'] = $value[8];
        
        $data['kode_negara'] = $value[9];
        $data['kode_pajak'] = $value[10];
        $data['jumlah_bruto'] = $value[11];
        $data['jumlah_dpp'] = $value[12];
        $data['tanpa_npwp'] = $value[13];
        
        $data['tarif'] = $value[14];
        $data['jumlah_pph'] = $value[15];
        $data['npwp_pemotong'] = $value[16];
        $data['nama_pemotong'] = $value[17];
        $data['tanggal_bukti_potong'] = $this->apps->dateConvert(str_replace('/','-',$value[18]));
        
        
        $query = $this->db->get_where('tran_csv_pph_21', $data);
        if($query->num_rows()>0){
            return false;
        }else{
            return $data;   
        }         
    }
}
