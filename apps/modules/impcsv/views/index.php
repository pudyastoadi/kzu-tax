<?php

/* 
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

?>   
<div class="row">
    <!-- left column -->
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{msg_main}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php
            $attributes = array(
                'role=' => 'form'
                , 'id' => 'form_add'
                , 'name' => 'form_add'
                , 'enctype' => 'multipart/form-data');
            echo form_open($submit,$attributes); 
        ?> 
          <div class="box-body">
          <?php
            $statsubmit = $this->session->userdata('statsubmit');
            $this->session->unset_userdata('statsubmit');
            if($statsubmit){
                $stat = json_decode($statsubmit);
                if($stat->state=="1"){
                    // '<div class="alert alert-warning alert-dismissible fade in" role="alert"> 
                    // <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    // <span aria-hidden="true">×</span></button> 
                    // <strong>Holy guacamole!</strong> Best check yo self, youre not looking too good. 
                    // </div>';
                    echo '<div class="alert alert-success alert-dismissible fade in" role="alert"> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Sukses! </strong> '.$stat->msg.'
                        </div>';
                }else{
                    echo '<div class="alert alert-danger alert-dismissible fade in" role="alert"> 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <strong>Peringatan! </strong> '.$stat->msg.'
                        </div>';
                }
            }
          ?>
              <div class="row">
                  <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputFile">Pilih File CSV</label>
                          <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i> 
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="csvfile" id="csvfile" accept=".csv">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                          </div>
                        </div>   
                  </div>
                  <div class="col-lg-12">
                      
                  </div>
              </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" name="submit" value="html">
                <i class="fa fa-cloud-upload"></i> Upload
            </button>
            <a href="<?php echo $reload;?>" class="btn btn-default">
                Batal
            </a>    
          </div>
        <?php echo form_close(); ?>
      </div>
      <!-- /.box -->
    </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        
    });
</script>