<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('persen'))
{      
    function persen($param) {
        $angka = (int) $param;
        if ($angka > 0) {
            return number_format($angka,'2',',','.');
        } else {
            return $angka;
        }                   
    
    }        
}
/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

