-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2019 at 02:15 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbkzutax`
--

-- --------------------------------------------------------

--
-- Table structure for table `tran_csv_pph_21`
--

CREATE TABLE `tran_csv_pph_21` (
  `masa_pajak` varchar(12) NOT NULL,
  `thn_pajak` varchar(5) NOT NULL,
  `pembetulan` varchar(2) NOT NULL,
  `no_bukti_potong` varchar(30) NOT NULL,
  `npwp` varchar(30) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `wp_luar_negeri` varchar(2) NOT NULL,
  `kode_negara` varchar(5) NOT NULL,
  `kode_pajak` varchar(15) NOT NULL,
  `jumlah_bruto` bigint(20) NOT NULL,
  `jumlah_dpp` bigint(20) NOT NULL,
  `tanpa_npwp` varchar(2) NOT NULL,
  `tarif` varchar(4) NOT NULL,
  `jumlah_pph` bigint(20) NOT NULL,
  `npwp_pemotong` varchar(30) NOT NULL,
  `nama_pemotong` varchar(50) NOT NULL,
  `tanggal_bukti_potong` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
