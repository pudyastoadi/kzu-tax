# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* KZU TAX - Web Application
* (KARYA ZIRANG UTAMA TAX)
* Version 1.0
* Author: 	Pudyasto Adi
* Website: 	http://www.pudyastoadi.my.id/
* Contact: 	mr.pudyasto@gmail.com
* Follow: 	https://twitter.com/pudyastoadi
* Like: 	https://www.facebook.com/dhyaz.cs

### How do I get set up? ###

* Install database
* Copy all *.copy.php into general files *.php
      - config/config.copy.php
      - config/database.copy.php
* Customize your setting
      - config/config.php
      - config/database.php
* Try your application

### Contribution guidelines ###

* AdminLTE - https://almsaeedstudio.com/themes/AdminLTE/index2.html
* Codeigniter 3 - https://codeigniter.com/
* HMVC - https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
* Template Engine - https://github.com/philsturgeon/codeigniter-template

### Who do I talk to? ###

* mr.pudyasto@gmail.com


### IMPORTANT ###
* ---------------
